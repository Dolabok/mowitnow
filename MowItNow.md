# MowItNow

### Lunch  

manual

```sh
$ gradle man
```

With default file
```sh
$ gradle run
```

With custom file 

```sh
$ gradle runWithFile -Pfile="your_file_here"
```

lunch test

```sh
$ gradle test
```
