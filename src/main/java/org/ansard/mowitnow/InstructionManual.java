package org.ansard.mowitnow;

public class InstructionManual {

    public static void main(String[] args) {

        System.out.println("Welcome to MowItNow \n");
        System.out.println("How to use ? \n\n");

        System.out.print("For run default file, run : ");
        String boldGrayLine = "\033[1mgradle run\n\033[0m";
        System.out.println(boldGrayLine);

        System.out.print("For run your own file, run : ");
        boldGrayLine = "\033[1mgradle runWithFile -Pfile=\"path_to_your_file\"\n\033[0m";
        System.out.println(boldGrayLine);

        System.out.print("For run test, run : ");
        boldGrayLine = "\033[1mgradle test\n\n\033[0m";
        System.out.println(boldGrayLine);

        System.out.println("How to write your instruction file ?\n");

        System.out.print("Line 1 : ");
        boldGrayLine = "\033[1mX Y\033[0m";
        System.out.println(boldGrayLine);
        System.out.println("Size of your zone (number)\n");

        System.out.println("For each lawn mower  : \n");

        System.out.print("A start position line. ex : ");
        boldGrayLine = "\033[1mX Y O\033[0m";
        System.out.println(boldGrayLine);
        System.out.println("With :");
        System.out.println("X/Y : Starting coordinate (number)");
        System.out.println("O : Orientation of mower (accept : N, E, W, S)\n");

        System.out.print("An instruction line. ex : ");
        boldGrayLine = "\033[1mGAGAGAGAA\033[0m";
        System.out.println(boldGrayLine);
        System.out.println("Different order for the mower");
        System.out.println("Accepted : ");
        System.out.println("G : turn left (on place)");
        System.out.println("D : turn right (on place)");
        System.out.println("A : go forward (on direction)");
    }
}
