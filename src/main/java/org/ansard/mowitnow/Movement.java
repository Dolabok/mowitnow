package org.ansard.mowitnow;

public class Movement {

    private int x, y;
    private final int maxX, maxY;
    private char o;
    private final String instruction;

    /**
     *
     * @param mapSizes X Y size of the area to be mowed
     * @param startPosition starting position of the mower
     * @param instruction Sequence of commands to be executed by the lawn mower
     */
    public Movement(String[] mapSizes, String[] startPosition, String instruction) {
        this.maxX = Integer.parseInt(mapSizes[0]);
        this.maxY = Integer.parseInt(mapSizes[1]);

        this.x = Integer.parseInt(startPosition[0]);
        this.y = Integer.parseInt(startPosition[1]);
        this.o = startPosition[2].charAt(0);

        this.instruction = instruction;
    }

    /**
     * execute orders and return the final position
     * @return final position of the mower
     */
    public String start() {

        for (char ch: this.instruction.toCharArray()) {
            switch(ch) {
                case 'G': left(); break;
                case 'D': right(); break;
                case 'A': forward(); break;
            }
        }
        // format result for stdout
        return (String.valueOf(this.x).concat(" ").concat(String.valueOf(this.y)).concat(" ").concat(String.valueOf(this.o)));
    }

    /**
     * method for executing the Left order (G)
     */
    private void left() {
        switch(this.o) {
            case 'N': o = 'W'; break;
            case 'W': o = 'S'; break;
            case 'S': o = 'E'; break;
            case 'E': o = 'N'; break;
        }
    }

    /**
     * method for executing the Right order (D)
     */
    private void right() {
        switch(this.o) {
            case 'N': o = 'E'; break;
            case 'E': o = 'S'; break;
            case 'S': o = 'W'; break;
            case 'W': o = 'N'; break;
        }
    }

    /**
     * method for executing the Forward order (A)
     */
    private void forward() {
        switch(this.o) {
            case 'N': y++; break;
            case 'E': x++; break;
            case 'W': x--; break;
            case 'S': y--; break;
        }

        // Correction if the position is out of the limits of the zone
        if ( this.x > this.maxX) { this.x = this.maxX; }
        if ( this.x < 0) { this.x = 0; }
        if ( this.y > this.maxY) { this.y = this.maxY; }
        if ( this.y < 0) { this.y = 0; }
    }
}