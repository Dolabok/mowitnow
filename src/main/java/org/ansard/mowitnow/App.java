package org.ansard.mowitnow;

import java.util.ArrayList;
import java.util.List;

public class App {
    /**
     * Takes as input an instruction file for all lawn mowers and returns their final positions
     * @param args or null
     * Path to Custom Filename
     * @throws Exception NoSuchFileException, AccessDeniedException, file empty or MowFileException
     */
    public static void main(String[] args) throws Exception {

        String filePath;

        // Get file name if provided
        if( args.length == 1 ) {
            filePath = args[0];
        }
        else {
            filePath = "src/main/resources/instructionFiles/default";
        }

        // load file data
        List<String> data =  new FileLoader(filePath).load();

        // get the size of the map
        String[] mapSizes = data.get(0).split(" ");

        List<String> result = new ArrayList<>();

        // Start moving lawnmowers
        for(int i=1; i<data.size(); i+=2) {
            result.add(new Movement(mapSizes, data.get(i).split(" "), data.get(i+1)).start());
        }

        // print results
        for (String line: result) {
            System.out.println(line);
        }
    }
}
