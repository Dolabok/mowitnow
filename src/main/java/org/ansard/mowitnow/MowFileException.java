package org.ansard.mowitnow;

class MowFileException extends Exception
{
    private final String filename;
    private final int line;

    public MowFileException(String filename, int line)
    {
        this.filename = filename;
        this.line = line;
    }

    @Override
    public String getMessage() {
        return "Error in file ".concat(this.filename).concat(" at line ").concat(String.valueOf(this.line));
    }
}