package org.ansard.mowitnow;

import java.io.IOException;
import java.nio.file.*;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class FileLoader
 * Takes as input the path to the file
 * Return the file data or an error
 */
public class FileLoader {
    private static final String FILE_NOT_FOUND = "The file provided is not found";
    private static final String ACCESS_DENIED = "Access to the file provided is denied";
    private static final String FILE_EMPTY = "The file is empty";

    private final String filePath;

    public FileLoader(String filePath) {
        this.filePath = filePath;
    }

    /**
     * returns the data of the file or an error if the file is empty
     * @return data from file
     * @throws Exception file empty
     */
    public List<String> load() throws Exception {

        List<String> data = getDataFormFile();

        assert data != null;
        if(data.isEmpty()) {
            throw new Exception(FILE_EMPTY);
        }

        fileChecker(data);

        return data;
    }

    /**
     *
     * @return file data or error if file is not found or access is denied
     * @throws IOException NoSuchFileException or AccessDeniedException
     */
    private List<String> getDataFormFile() throws Exception {

        try {
            Path path = Paths.get(this.filePath);
            return Files.readAllLines(path);
        } catch(Exception e) {
            error(e);
            return null;
        }
    }

    /**
     * The fileChecker class checks the file patern line by line and return MowFileException if it is not the case
     * @param data lines of the files
     * @throws Exception MowFileException
     */
    private void fileChecker(List<String> data) throws Exception {

        Pattern firstLinePattern = Pattern.compile("^[0-9]*? [0-9]*?$");
        Pattern startPositionLinePattern = Pattern.compile("^[0-9]*? [0-9]*? [NWES]$");
        Pattern moveInstructionLinePattern = Pattern.compile("^[GDA]*?$");

        boolean matchFirstLine;
        boolean matchStartPosition;
        boolean matchMoveInstruction;

        Matcher matcherFirstLine = firstLinePattern.matcher(data.get(0));
        matchFirstLine = matcherFirstLine.find();

        if(!matchFirstLine) {
            throw new MowFileException(this.filePath, 1);
        }

        for(int i=1; i<data.size(); i+=2) {

            Matcher matcherStartPosition = startPositionLinePattern.matcher(data.get(i));
            matchStartPosition = matcherStartPosition.find();

            if (!matchStartPosition) {
                throw new MowFileException(this.filePath, i);
            }

            Matcher matcherMoveInstruction = moveInstructionLinePattern.matcher(data.get(i+1));
            matchMoveInstruction = matcherMoveInstruction.find();

            if (!matchMoveInstruction) {
                throw new MowFileException(this.filePath, i+1);
            }
        }
    }

    private static void error(Exception e) throws Exception {
        if(e instanceof NoSuchFileException) {
            throw new NoSuchFileException(FILE_NOT_FOUND);
        }
        else if (e instanceof AccessDeniedException) {
            throw new AccessDeniedException(ACCESS_DENIED);
        }
        else {
            throw new Exception(e);
        }
    }
}
